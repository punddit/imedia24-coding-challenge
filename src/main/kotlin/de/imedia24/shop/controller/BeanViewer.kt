package de.imedia24.shop.controller

import org.slf4j.LoggerFactory
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
internal class BeanViewer {
    private val LOG = LoggerFactory.getLogger(javaClass)
    @EventListener
    fun showBeansRegistered(event: ApplicationReadyEvent) {
        val beanNames = event.applicationContext
            .beanDefinitionNames
        for (beanName in beanNames) {
            LOG.info("{}", beanName)
        }
    }
}