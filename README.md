
### Building the docker image
> building the project
```
./gradlew clean build -x test
```
>building the docker image tagged: imedia24/imedia24-shop
```
docker build --build-arg JAR_FILE=build/libs/\*.jar -t imedia24/imedia24-shop .
```
>running the docker image
```
docker run -t -p 8080:8080 imedia24/imedia24-shop
```
* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

